module.exports = {
	"title": "Jon Park",
	"url": "https://jonpark.co.uk/",
	"language": "en-GB",
	"description": "Yet another Web developer.",
	"author": {
		"name": "Jon Park",
		"email": "hey@jonpark.co.uk",
		"url": "https://jonpark.co.uk/about-me/"
	}
}
