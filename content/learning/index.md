---
layout: layouts/page.njk
eleventyNavigation:
  key: Learning
  order: 5
title: Learning
updatedAt: 2023-02-26T17:27:36
---

No one ever stops learning, we practice things all the time. Here's what I'm currently putting some effort into getting better at.

## Development

- <abbr title="Open ID Connect">OIDC</abbr>. I need an authentication service for [Howdee](https://howdee.io/), so I'm looking at something formal. There's a lot of terminology to re-learn.
- How to proxy Node services with SSL in a local environment. As part of the OIDC work I'm doing, I need a way to run my development environment on an `https://` address.
- A big refresh of accessibility best practice using [Sara Soueidan's](https://sarasoueidan.dev/@SaraSoueidan) fantastic [Practical Accessibility](https://practical-accessibility.today/) video course.

## Business

We've recently (January 2023) re-launched the website for [Increment By One](https://incrementby.one/). For most of its existence, I've relied on a personal network of recommendations to keep work coming through the doors. This marks the first time we've had a full website, so I'm trying to learn how to do more marketing.

## Reading

I'm currently reading "[Engineering Management for the Rest of Us](https://www.engmanagement.dev/)" by [Sarah Drasner](https://sarahdrasnerdesign.com/).

## Languages

I'm an introvert with social anxiety, but I do enjoy awkwardly not speaking to people in several languages.

I got assigned to the <span aria-hidden>🇫🇷</span> French group of students when I started secondary (high) school and took it through (what was then) AS Level. I'm not fluent, but I can get by and I'm building on it still.

When Maria joined our team at [Increment By One](https://incrementby.one/about#meet-the-team), I picked up the odd phrase of <span aria-hidden>🇪🇸</span> Spanish. My son took an interest, so I started studying more to help him when he gets stuck. Our work chat gets peppered with simple phrases sometimes. "<i lang="es">¿Necesitas ayuda?</i>" ("Do you need help?") and "<i lang="es">Necesito una taza de té</i>" ("I need a cup of tea") being some of the more common ones. I'm only starting out, but might be able to handle a café situation.

I tried learning some <span aria-hidden>🇯🇵</span> Japanese after I left university. It was an in-person class and the anxiety got a little much, so I put it down. Since our household all got into language-learning, I wanted to add something that was vastly different to English. I started again and know some phrases, but still regularly get the exercises wrong. I would really like to visit some time.

If you have an account, you can [follow me on Duolingo here](https://www.duolingo.com/profile/jonspark). If you need one, here's my [invite link]().
