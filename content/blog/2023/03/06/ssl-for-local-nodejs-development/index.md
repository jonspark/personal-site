---
title: SSL for Local NodeJS Development
description: Using a local proxy server for your Node services.
date: 2023-03-06T19:36:23
tags:
  - Development
  - Platform
  - NodeJS
---

More often than not, running a NodeJS process on a port of localhost is enough for your development environment. If you ever need more than one service running at the same time, you can set a unique port for each of them. `localhost:3000` for App 1, `localhost:3001` for App 2, and so on.

I've recently started working on an authentication service for a set of applications, and I chose Open ID Connect (OIDC) to do it. It's a pretty big topic, but written into the specification is a requirement that redirect URLs have to start with `https`.

I could deploy these to a test server on a hosting provider, that would work. I'm still learning and making small, quick changes, so waiting for the deploy each time would slow things down. Until the service is stable, running everything locally would speed the process up a lot.

## The Approach

The idea here is to run a lightweight web server on `https` and have it act as an intermediary to our usual locally-running Node process. This is very similar to how a production environment works - the approach is called "proxying", making the server doing it a "proxy server". I'm going to use [Caddy](https://caddyserver.com/) for this.

We can't use a free [Let's Encrypt](https://letsencrypt.org/) certificate to secure our server, and a self-signed certificate will be flagged by Node and the browser as insecure. Instead, we'll create our own local Certificate Authority and generate a certificate that way.

{% wyltkm "What is a Certificate Authority?" %}
A Certificate Authority (or Certification Authority) is a cryptography term for something that stores, generates, and issues digital certificates. In this case, it's the certificate we need to allow our proxy server to handle <abbr title="HyperText Transfer Protocol Secure">HTTPS</abbr> requests.

There are a small number of large, multi-national Certificate Authorities, and our browsers and operating systems will only trust the ones that they know about.

Cryptography in general is a massive topic, but you don't need to know huge amounts to use it. If you're interested, the [Certificate Authority Wikipedia page](https://en.wikipedia.org/wiki/Certificate_authority) has a good overview.
{% endwyltkm %}

## Install the Web Server

[Caddy](https://caddyserver.com/) is designed to be SSL by default, so it will try to use [Let's Encrypt](https://letsencrypt.org/) when it starts. We'll deal with that in the next step. For now, we'll install it using [Homebrew](https://brew.sh/).

```shell
brew install caddy
```

## Create a local Certificate Authority

Next, we need to install a tool called [mkcert](https://github.com/FiloSottile/mkcert). This tool will install a local, trusted <abbr title="Certificate Authority">CA</abbr> and we can use it to create our own certificates. There are [installation instructions](https://github.com/FiloSottile/mkcert#installation) (including Windows) on the [GitHub project](https://github.com/FiloSottile/mkcert).

If you're using [Firefox](https://www.mozilla.org/en-GB/firefox/developer/), like I am, you'll also need [NSS](https://formulae.brew.sh/formula/nss).

```shell
brew install mkcert
brew install nss # Only for Firefox
```

Once installed, run the `-install` action to create a local <abbr title="Certificate Authority">CA</abbr> in your system trust store. It will create a root certificate and you should pay close attention to the [warning about not sharing it](https://github.com/FiloSottile/mkcert#installation).

```shell
mkcert -install
```

## Restart your Browser

This might only be specific to Firefox, because of the additional install requirement.

## Create a Certificate

I'm going to do this in my project folder, because running this next command creates the certificate in the current directory.

I'll be using `howdee.test` throughout this example, so swap this out for your own domain. I need subdomains for my purposes, but you could use different domain names for each service.

```shell
cd ~/PATH_TO_MY_PROJECT
mkcert "*.howdee.test"
```

I don't want these in the root of my project, so I'm going to put them in a folder.

```shell
mkdir -p .caddy/certs
mv _wildcard.howdee.test* ./.caddy/certs
```

## Create a `caddyfile`

Caddy can be used purely with command line arguments, but it also offers a `caddyfile` as a small, convenient configuration file. If you're familiar with other servers, it's like setting up a `<VirtualHost>` in Apache server or a Server Block in nginx.

I'm going to put this in the project's `./.caddy` folder. In it, we'll define our two local sites, proxy them to our Node services running on each port, and give it the certificate it needs to secure the connection.

```nginx
# Our main Web application
app.howdee.test {
  # Our App runs on port 3001, forward everything there
	reverse_proxy localhost:3001

  # Path to my App service public folder
	root * ./apps/app/public

  # Serve files like images, CSS files, and other assets
	file_server

  # Specify the certificate to use
	tls ./certs/_wildcard.howdee.test.pem ./certs/_wildcard.howdee.test-key.pem
}

# The Authentication service running an OIDC Provider
auth.howdee.test {
  # Our Auth service runs on port 3002, forward everything there
	reverse_proxy localhost:3002

  # Path to my Auth service public folder
	root * ./apps/auth/public

  # Serve files like images, CSS files, and other assets
	file_server

  # Specify the certificate to use
	tls ./certs/_wildcard.howdee.test.pem ./certs/_wildcard.howdee.test-key.pem
}
```

## Point our `.test` domains to localhost

If you haven't put anything else in place, you'll need to edit your `/etc/hosts` file to point your test domains to localhost. This will stop your browser going to the wider Web to look for an address that doesn't exist.

```diff-ini
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1       localhost
255.255.255.255 broadcasthost
::1             localhost

+127.0.0.1 app.howdee.test
+127.0.0.1 auth.howdee.test
```

{% wyltkm 'Using Dnsmasq for a local development <abbr title="Top Level Domain">TLD</abbr>' %}
You may work on many projects and managing `/etc/hosts` can be a little awkward. There are tools to make this easier, but you could also use a local DNS server like [Dnsmasq](https://thekelleys.org.uk/dnsmasq/doc.html) for a local-only <abbr title="Top Level Domain">TLD</abbr>.

I've used the instructions from this blog post on each new computer I've had for a good few years. You should note the caveat that the world has moved on since it was written and `.dev` is now a real <abbr title="Top Level Domain">TLD</abbr>. Use `.test` instead.

[Using Dnsmasq for local development on OS X](https://passingcuriosity.com/2013/dnsmasq-dev-osx/)
{% endwyltkm %}

## Using in a Node project

Now, when you're working on your project, you can run the server alongside your local processes. So, if you have your Node processes running in on terminal tab, in another you'd run:

```bash
# From your project root folder...
cd .caddy
caddy run
```

This is will start Caddy in your terminal using the `caddyfile` to define the servers above. If you now visit your local domain, you should get a response from a secured HTTPS connection.

If you hover over the padlock, or view the secure link information, you should see that it is verified by our "mkcert development CA".

{% image "./firefox-app-dot-howdee-dot-test-screenshot.png", "" %}

{% wyltkm "A potential 'gotcha' I totally got caught by." %}
At some point whilst writing this post, I started Caddy as a background task (using `caddy start`). I was able to start and stop a separate Caddy process using `caddy run`, as described above, without noticing a problem.

Later, when I'd forgotten about what I was doing, I started to work on a different, unrelated project and wondered why my local `.test` domain was redirecting to `https`.

After more than a few minutes of trying to debug the problem, I stumbled on what I had done. Because Caddy uses HTTPS automatically by default, it was intercepting my HTTP request and redirecting it to HTTPS.

Running `caddy stop` fixed my problem.
{% endwyltkm %}

## Running remote calls from NodeJS

Running this in the browser was fine, but my app runs a remote request to one of my secure services - like an asynchronous `fetch` request. I was getting an error along these lines:

```bash
Error: unable to verify the first certificate
    at TLSSocket.onConnectSecure (node:_tls_wrap:1540:34)
    at TLSSocket.emit (node:events:513:28)
    at TLSSocket._finishInit (node:_tls_wrap:959:8)
    at ssl.onhandshakedone (node:_tls_wrap:743:12) {
  code: 'UNABLE_TO_VERIFY_LEAF_SIGNATURE'
}
```

Node has its own set of trusted certificate authorities and doesn't automatically know about our's. You can make it aware by putting the path to your `mkcert` root certificate in an environment var.

I added the following line to my `.env` file, restarted the service, and it all worked correctly.

```ini
NODE_EXTRA_CA_CERTS: '/Users/{YOUR_MACOS_USER}/Library/Application Support/mkcert/rootCA.pem'
```

{% wyltkm 'Finding the mkcert root certificate path' %}
mkcert will install a root certificate when you first set it up. This won't be in your project folder, but you can find it with a built-in command.

```shell
mkcert -CAROOT
```

{% endwyltkm %}
