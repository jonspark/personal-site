---
layout: layouts/page.njk
# eleventyNavigation:
#   key: Uses
#   order: 6
title: Uses
draft: true
# updatedAt: 2023-02-20T16:04:06
---

It may have originated there, I'm not sure, but I first saw a "Uses" page on [Wes Bos's site](https://wesbos.com/uses) and thought it was a nice way to share a set up.

## In This Site

This site is built using [11ty](https://11ty.dev), it's the first time I've used it and it's been quite nice. The theme is custom, if simple, but written using [Nunjucks](https://mozilla.github.io/nunjucks/) and plain CSS. The code block syntax highlighting comes out of the box with 11ty and uses [PrismJS](https://prismjs.com/).

The font is [Atkinson Hyperlegible](https://brailleinstitute.org/freefont) from the [Braille Institute](https://brailleinstitute.org/).

The code is public and available on [GitLab](https://gitlab.com/jonspark/website).

## In Development

- [Visual Studio Code](https://code.visualstudio.com/) with Wes Bos's [Cobalt2 Theme](https://marketplace.visualstudio.com/items?itemName=wesbos.theme-cobalt2) and [Victor Mono](https://rubjo.github.io/victor-mono/) with font ligatures.
- [Firefox Developer Edition](https://www.mozilla.org/en-GB/firefox/developer/) as a day-to-day browser.
- [iTerm 2](https://iterm2.com/) for a terminal with [Oh My ZSH](https://ohmyz.sh/) and the [PowerLevel10K](https://github.com/romkatv/powerlevel10k) theme.
- [Polypane](https://polypane.app/) for testing multiple breakpoints at once.

## On My Desk

I use a 2021 M1 iMac in my home office and use it as my main display. I got the version that doesn't have a stand and have it attached to a fairly standard monitor arm. As much as I like a big screen, you can't get better than how those HiDPI settings look.

To my left, I have an [LG DualUp 28MQ780](https://www.lg.com/uk/monitors/lg-28mq780-b). It's a bit strange, I've never come across anything else like it, but it's great for big code files or when you need to check lots of breakpoints in Polypane.
