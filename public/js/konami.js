document.addEventListener('DOMContentLoaded', function () {
	const konamiCode = [
		'ArrowUp',
		'ArrowUp',
		'ArrowDown',
		'ArrowDown',
		'ArrowLeft',
		'ArrowRight',
		'ArrowLeft',
		'ArrowRight',
		'b',
		'a',
	];

	let position = 0;
	document.addEventListener('keydown', (ev) => {
		const { key } = ev;
		const nextKey = konamiCode[position];

		if (key !== nextKey) {
			position = 0;
		} else {
			position += 1;
			console.log(key);

			if (position === konamiCode.length) {
				console.log('Cheats activated!');
				position = 0;
			}
		}
	});
});
